'use strict';

const util = require('util');
const fs = require('fs');
const path = require('path');

const readFile = util.promisify(fs.readFile);
const readDir = util.promisify(fs.readdir);

async function readFiles(dirname, extension) {
  let totalNumsLinesInAllFiles = 0;
  try {
    const filenames = await readDir(dirname);
    const suitableFiles = filenames.filter(filename => {
      if (path.extname(filename) === `.${extension}`) {
        return filename;
      }
    });

    const promises = suitableFiles.map(async filename => {
      const content = await readFile(dirname + filename, 'utf-8');
      const numLinesInFile = content.split('\n').length - 1;
      totalNumsLinesInAllFiles += numLinesInFile;
      console.log(`count of lines in ${filename}: ${numLinesInFile}`);
      return numLinesInFile;
    });
    await Promise.all(promises);
  } catch (err) {
    console.error(err);
  }
  return totalNumsLinesInAllFiles;
}

module.exports = readFiles;
