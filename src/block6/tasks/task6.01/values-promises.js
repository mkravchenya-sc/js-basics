'use strict';

const attachTitle = line => `DR. ${line}`;

const promise = Promise.resolve('MANHATTAN');

promise.
  then(attachTitle).
  then(console.log);
