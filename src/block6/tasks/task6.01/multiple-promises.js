'use strict';

const all = (promise1, promise2) => {
  return new Promise((resolve, reject) => {
    let count = 0;
    const values = [];
    const promises = [promise1, promise2];
    promises.forEach((promise, i) => {
      promise.then(result => {
        count++;
        values[i] = result;
        if (count === 2) {
          resolve(values);
        }
      })
    })
  });
}

// eslint-disable-next-line no-undef
all(getPromise1(), getPromise2()).then(console.log);
