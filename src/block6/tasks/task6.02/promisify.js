'use strict';

const fs = require('fs');
const util = require('util');

const readerWrapper = util.promisify(fs.readFile);

module.exports = readerWrapper;
