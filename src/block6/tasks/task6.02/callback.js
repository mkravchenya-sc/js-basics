'use strict';

const fs = require('fs');

function read(path, callback) {
  fs.readFile(path, 'utf8', callback);
}

module.exports = read;
