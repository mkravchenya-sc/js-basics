'use strict';

const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);

async function asyncRead(filePath) {
  let content;
  try {
    content = await readFile(filePath, 'utf8');
    console.log(content);
  } catch (e) {
    console.error('error: ', e);
  }
  return content;
}

module.exports = asyncRead;
