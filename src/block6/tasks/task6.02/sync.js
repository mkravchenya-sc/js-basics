'use strict';

const fs = require('fs');

const read = path => fs.readFileSync(path, 'utf8');

module.exports = read;
