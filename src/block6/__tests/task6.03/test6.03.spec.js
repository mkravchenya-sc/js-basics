'use strict';

const path = require('path');
const readFiles = require(path.join(__dirname, '..', '..', 'tasks', 'task6.03', 'read-files.js'));
const { expect } = require("chai");

describe('task6.03', () => {
  const expected = 9;
  const dirPath = path.join(__dirname, 'resources/');

  it('Actual value should be as expected', async () => {
    const actual = await readFiles(dirPath, 'txt');
    expect(actual).to.equal(expected);
  });
});
