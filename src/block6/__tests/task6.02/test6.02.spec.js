'use strict';

const path = require('path');
const syncRead = require('../../tasks/task6.02/sync.js');
const callbackRead = require('../../tasks/task6.02/callback.js');
const promisifyRead = require('../../tasks/task6.02/promisify.js');
const asyncRead = require('../../tasks/task6.02/async-await.js');
const { assert, expect } = require("chai");

describe('task6.02', () => {
  const expected = 'Hello world!\nGoodbye world!\n';
  const filePath = path.join(__dirname, 'resources', 'text.txt');

  it('Sync: actual value should be as expected', () => {
    assert.strictEqual(syncRead(filePath), expected);
  });

  it('Callback: actual value should be as expected', (done) => {
    callbackRead(filePath, (error, content) => {
      expect(content).to.equal(expected);
      done();
    });
  });

  it('Promisify: actual value should be as expected', async () => {
    const result = await promisifyRead(filePath, 'utf8');
    expect(result).to.equal(expected);
  });

  it('Async/await: actual value should be as expected', async () => {
    const result = await asyncRead(filePath);
    expect(result).to.equal(expected);
  });
});
