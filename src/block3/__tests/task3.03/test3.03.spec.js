'use strict';

const bank = require('../../tasks/task3.03/bank');
const { assert } = require('chai');

describe('task3.03', () => {
  it('The user1 module must be connected', () => {
    const user = bank.connectModule(0);
    assert.strictEqual('user1', user.userName);
  });

  it('The user2 module must be connected', () => {
    const user = bank.connectModule(1);
    assert.strictEqual('user2', user.userName);
  });
});
