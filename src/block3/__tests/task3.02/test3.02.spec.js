'use strict';

const banker = require('../../tasks/task3.02/banker');
const banker1 = banker();
const banker2 = banker();
const { assert } = require('chai');

describe('task3.02', () => {
  it('Banker2 actions should not change the bank variable for banker1', () => {
    const initialBank = banker1.getBank();
    banker2.add(4);
    const actualBank = banker1.getBank();
    assert.strictEqual(initialBank, actualBank);
  });
});
