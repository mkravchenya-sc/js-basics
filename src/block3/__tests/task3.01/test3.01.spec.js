'use strict';

const { assert } = require('chai');

describe('task3.01', () => {
  describe('Checking the correct operation of the banker methods', () => {
    const banker = require('../../tasks/task3.01/banker');

    it('The value of the bank variable must correspond to the initial value', () => {
      const initialBank = banker.getBank();
      banker.add(4);
      const actualBank = banker.getBank();
      assert.notStrictEqual(initialBank, actualBank);
    });

    it('The difference between the initial and final values of the bank must correspond to the expected value', () => {
      const initialBank = banker.getBank();
      const expected = 4;
      banker.add(expected);
      const actualBank = banker.getBank();
      assert.strictEqual(actualBank - initialBank, expected);
    });

    it('Value must be reset to 0', () => {
      const expected = 0;
      banker.add(13);
      banker.reset();
      const actualBank = banker.getBank();
      assert.strictEqual(actualBank, expected);
    });
  });

  describe('Checking the impact of banker user1 and user2 on the common variable bank', () => {
    const banker1 = require('../../tasks/task3.01/banker');
    const banker2 = require('../../tasks/task3.01/banker');

    it('Banker2 actions should change the bank variable for banker1', () => {
      const initialBank = banker1.getBank();
      banker2.add(4);
      const actualBank = banker1.getBank();
      assert.notStrictEqual(initialBank, actualBank);
    });
  });
});
