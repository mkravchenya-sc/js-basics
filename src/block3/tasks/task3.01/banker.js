'use strict';

let bank = 0;

exports.display = () => {
  console.log(`bank = ${bank}`);
};

exports.add = (num) => {
  bank += num;
};

exports.reset = () => {
  bank = 0;
};

exports.getBank = () => bank;
