'use strict';

const user1 = require('./user1');
const user2 = require('./user2');

user1.perform();
user2.perform();
