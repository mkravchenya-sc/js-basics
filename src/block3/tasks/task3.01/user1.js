'user strict';

const banker = require('./banker.js');

exports.perform = () => {
  console.log('The banker started working with user1');
  banker.display();

  for (let i = 0; i <= 3; i++) {
    banker.add(i);
  }

  banker.display();
  console.log('The banker finished working with user1');
}
