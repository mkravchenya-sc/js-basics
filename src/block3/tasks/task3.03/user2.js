'user strict';

const banker = require('./banker.js')();

exports.userName = 'user2';

exports.perform = () => {
  console.log('The banker started working with user2');
  banker.display();

  for (let i = 1; i <= 5; i++) {
    banker.add(i);
    banker.display();
    banker.reset();
  }

  banker.display();
  console.log('The banker finished working with user2');
}
