'use strict';

exports.connectModule = (userCount) => {
  return userCount === 0 ? require(__dirname + '/user1') : require(__dirname + '/user2');
}
