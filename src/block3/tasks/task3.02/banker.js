'use strict';

module.exports = (() => {
  let bank = 0;

  return {
    display: () => {
      console.log(`bank = ${bank}`);
    },

    add: num => {
      bank += num;
    },

    reset: () => {
      bank = 0;
    },

    getBank: () => bank
  }
});
