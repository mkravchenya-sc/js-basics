'use strict';

class Person {
  constructor(age, name) {
    this.age = age;
    this.name = name;
  }

  run() {
    console.log(`Person ${this.name} is running ...`)
  }
}


class Developer extends Person {
  constructor(age, name, language) {
    super(age, name);
    this.language = language;
  }

  code() {
    console.log(`Developer ${this.name} is coding with ${this.language} ...`);
  }
}

const developer = new Developer(27, 'Misha', 'Javascript');
developer.code();

module.exports = Developer;
