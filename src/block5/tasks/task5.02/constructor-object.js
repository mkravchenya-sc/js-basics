'use strict';

// eslint-disable-next-line no-new-object
const person = new Object();
person.age = 13;
person.name = 'name1';
person.run = function() {
  console.log(`Person ${this.name} is running ...`);
};

person.run();

module.exports = person;
