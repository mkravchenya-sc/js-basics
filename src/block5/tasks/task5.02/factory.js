'use strict';

function Person(options) {
  this.age = options.age;
  this.name = options.name;
}

Person.prototype.run = function() {
  console.log(`Person ${this.name} is running ...`);
};

function Developer(options) {
  Person.call(this, options);
  this.language = options.language;
}

function PersonFactory() {}

PersonFactory.prototype.personType = Person;

PersonFactory.prototype.createPerson = function(options) {
  switch (options.personType) {
    case 'person':
      this.personType = Person;
      break;
    case 'developer':
      this.personType = Developer;
      Developer.prototype = Object.create(Person.prototype);
      Developer.prototype.constructor = Developer;
      Developer.prototype.code = function() {
        console.log(`Developer ${this.name} is coding with ${this.language} ...`);
      }
      break;
    default:
      this.personType = Person;
      break;
  }

  // eslint-disable-next-line new-cap
  return new this.personType(options);
}

const personFactory = new PersonFactory();
const person = personFactory.createPerson({
  personType: 'developer',
  age: 27,
  name: 'Misha',
  language: 'java'
});

person.run();
person.code();

module.exports = person;
