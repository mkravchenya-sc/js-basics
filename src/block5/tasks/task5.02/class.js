'use strict';

class Person {
  constructor(age, name) {
    this.age = age;
    this.name = name;
  }

  run() {
    console.log(`Person ${this.name} is running ...`)
  }
}

const person = new Person(16, 'name5');
person.run();

module.exports = person;
