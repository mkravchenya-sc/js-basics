'use strict';

const person = {
  age: 13,
  name: 'name2',
  run: function() {
    console.log(`Person ${this.name} is running ...`)
  }
};

person.run();

module.exports = person;
