'use strict';

function Person(age, name) {
  this.age = age;
  this.name = name;
}

Person.prototype.run = function() {
  console.log(`Person ${this.name} is running ...`)
}

const person = new Person(25, 'name4');
person.run();

module.exports = person;
