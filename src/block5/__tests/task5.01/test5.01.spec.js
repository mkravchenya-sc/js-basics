'use strict';

const { assert } = require('chai');

describe('task5.01', () => {
  const obj = require('../../tasks/task5.01/obj');

  it('The toString() method must be absent in the object', () => {
    assert.doesNotHaveAnyKeys(obj, ['toString']);
  });

  it('The object must have its own toString() method', () => {
    obj.toString = function() {
      return this.name;
    };
    assert.containsAllKeys(obj, ['toString']);
  });
});
