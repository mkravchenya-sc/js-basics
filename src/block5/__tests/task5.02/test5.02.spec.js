'use strict';

const { assert } = require('chai');

describe('task5.02', () => {
  const persons = [];
  persons.push(require('../../tasks/task5.02/constructor-object'));
  persons.push(require('../../tasks/task5.02/literal-notation'));
  persons.push(require('../../tasks/task5.02/factory'));
  persons.push(require('../../tasks/task5.02/function-constructor'));
  persons.push(require('../../tasks/task5.02/class'));

  const properties = ['age', 'name', 'run'];

  const makeTest = (person) => {
    it('Person must have relevant properties', () => {
      properties.forEach(property => {
        assert.property(person, property, `Error finding property + ${property}`);
      });
    });
  }

  persons.forEach(person => {
    makeTest(person);
  });
});
