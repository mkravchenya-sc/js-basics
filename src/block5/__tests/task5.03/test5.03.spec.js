'use strict';

const { assert } = require('chai');

describe('task5.03', () => {
  const Developer = require('../../tasks/task5.03/developer');
  const properties = ['age', 'name', 'run', 'language', 'code'];

  const dev = new Developer(27, 'Misha', 'Java');

  it('Developer must have relevant properties', () => {
    properties.forEach(property => {
      assert.property(dev, property, `Error finding property + ${property}`);
    });
  });
});
