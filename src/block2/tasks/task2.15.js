'use strict';

exports.sortPersons = obj => {
  const persons = obj['persons'];
  const sorterFieldName = obj['sorterFieldName'];
  const sorterFieldDirection = obj['sorterFieldDirection'];
  return persons.
    filter(person => person.hasOwnProperty(sorterFieldName)).
    sort((person1, person2) => {
      const property1 = person1[sorterFieldName];
      const property2 = person2[sorterFieldName];

      if (property1 === undefined) {
        return Infinity;
      } else if (property2 === undefined) {
        return -1;
      }

      let result = property1 > property2 ? 1 : (property1 < property2 ? -1 : 0);

      if (sorterFieldDirection === 'DESC' && result !== 0) {
        result = -result;
      }

      return result;
    });
}
