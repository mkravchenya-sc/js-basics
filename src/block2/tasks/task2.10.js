'use strict';

exports.toArrayLikeObjectDynamic = arrayToConvert => {
  const target = {};
  for (const key in arrayToConvert) {
    if (target.hasOwnProperty.call(arrayToConvert, key)) {
      target[key] = arrayToConvert[key];
    }
  }

  target.length = arrayToConvert.length;

  return target
}
