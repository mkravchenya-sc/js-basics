'use strict';

exports.isSameProperties = (obj1, obj2) => {
  if (Object.keys(obj1).length !== Object.keys(obj2).length) {
    return false;
  }

  for (const key1 in obj1) {
    if (!(key1 in obj2)) {
      return false;
    }
  }

  return true;
}
