'use strict';

exports.caslte = (a, b) => {
  // eslint-disable-next-line no-param-reassign
  [a, b] = [b, a];
  return [a, b];
}
