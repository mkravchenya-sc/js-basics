'use strict';

exports.lastDigitToString = num => +num.toString().slice(-1);

exports.lastDigit = num => {
  let convertedNum = Math.abs(num);
  let fract = convertedNum - Math.trunc(convertedNum);
  if (fract < 1) {
    while (fract !== 0) {
      convertedNum = (fract * 10).toFixed(8);
      fract = (convertedNum - Math.trunc(convertedNum));
    }
  }

  return convertedNum % 10;
}
