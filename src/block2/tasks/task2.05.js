'use strict';

exports.kick = (_ => {
  let goals = 0;
  return function() {return ++goals;};
})();
