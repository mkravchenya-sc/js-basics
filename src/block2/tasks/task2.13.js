'use strict';

exports.arrowAverage = (...args) => {
  if (args.length === 0) {
    return undefined;
  }

  let sum = 0;

  const isSumPossible = args.every((value) => {
    if (typeof value !== 'number' || isNaN(value)) {
      return false;
    }
    sum += value;
    return true;
  });

  return isSumPossible ? sum / args.length : undefined;
}
