'use strict';

exports.classicAverage = function() {
  let sum = 0;
  if (arguments.length === 0) {
    return undefined;
  }
  for (let i = 0; i < arguments.length; i++) {
    const value = arguments[i];
    if (typeof value !== 'number' || isNaN(value)) {
      return undefined;
    } else {
      sum += value;
    }
  }

  return typeof sum === 'number' ? sum / arguments.length : undefined;
}
