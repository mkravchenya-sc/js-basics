'use strict';
// Supervisor Code Review: OK.

exports.ageInWeeks = (birthday, relativeDate) => {
  const diff = relativeDate - birthday;
  const weeks = Math.floor(diff / (1000 * 60 * 60 * 24 * 7));
  return weeks;
}
