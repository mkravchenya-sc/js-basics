'use strict';

const castlingNameValue = obj => {
  const copy = {
    ...obj
  };
  for (const key in copy) {
    if (key !== copy[key]) {
      copy[copy[key]] = key;
      delete copy[key];
    }
  }

  return copy;
}

const doubleCastlingNameValue = obj => {
  let copy = {
    ...obj
  };
  for (let i = 0; i < 2; i++) {
    copy = castlingNameValue(copy);
  }

  return copy;
}

exports.castlingIfPossible = obj => {
  const copy = {
    ...obj
  };
  for (const key in copy) {
    if (key !== copy[key]) {
      const keyValue = {};
      keyValue[key] = copy[key];
      const doubleSwapObj = doubleCastlingNameValue(keyValue);
      if (doubleSwapObj[key] !== obj[key]) {
        return null;
      } else {
        const newKey = copy[key];
        copy[newKey] = key;
        delete copy[key];
      }
    }
  }

  return copy;
}
