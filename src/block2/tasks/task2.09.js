'use strict';

exports.getMethodsNames = obj => {
  const objNames = [];
  for (const key in obj) {
    if (typeof obj[key] === 'function') {
      objNames.push(key);
    }
  }

  return objNames;
}
