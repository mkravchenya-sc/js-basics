'use strict';

const { assert } = require('chai');

describe('task2.05', () => {
  const expectedValues = [3, 7, 13];

  const makeTest = (expected) => {
    it('The goals variable must match the expected value', () => {
      const task = require('../tasks/task2.05');
      const kick = task.kick;
      const calls = expected;
      let actualGoals = 0;

      [...Array(calls)].forEach(() => {
        actualGoals = kick();
      });

      assert.strictEqual(actualGoals, expected);
      delete require.cache[require.resolve('../tasks/task2.05')];
    });
  }

  expectedValues.forEach(expected => {
    makeTest(expected);
  });
});
