'use strict';

const task = require('../tasks/task2.06');
const { assert } = require('chai');

describe('task2.06', () => {
  const dates = [
    [new Date(2018, 1, 13), new Date(2016, 3, 24)],
    [new Date(1956, 7, 6), new Date(2015, 9, 25)],
    [new Date(2001, 11, 3), new Date(2004, 6, 21)]
  ]

  const makeTest = (values) => {
    it('After castling, the values should be swapped', () => {
      const date1 = values[0];
      const date2 = values[1];
      assert.deepEqual(task.caslte(date1, date2), [values[1], values[0]]);
    });
  }

  dates.forEach(values => makeTest(values));
});
