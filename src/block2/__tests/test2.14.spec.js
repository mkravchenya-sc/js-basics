'use strict';

const task = require('../tasks/task2.14');
const { assert } = require('chai');

describe('task2.14', () => {
  it('The product must match the expected value', () => {
    assert.strictEqual(task.f(13)(56)(25), 18200);
  });

  it('The product must match the expected value', () => {
    assert.strictEqual(task.f(3)(49)(76), 11172);
  });

  it('The product must match the expected value', () => {
    assert.strictEqual(task.f(-5)(43)(11), -2365);
  });

  it('The product must match the expected value', () => {
    assert.strictEqual(task.f(0.01)(21)(0.005), 0.00105);
  });
});
