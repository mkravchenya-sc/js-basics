'use strict';

const task = require('../tasks/task2.07');
const { assert } = require('chai');

describe('task2.07', () => {
  const objects = [
    [
      {
        name: 'name1', age: 5
      },
      {
        name: 'name2', age: 3
      },
      true
    ],
    [
      {
        name: 'name3', age: 5
      },
      {
        name: 'name4', age: 3, height: 188
      },
      false
    ],
    [
      {
        name: 'name5', age: 5
      },
      {
        name: 'name6', height: 188
      },
      false
    ]
  ];

  const makeTest = (obj1, obj2, expected) => {
    it(`Do obj1 ${JSON.stringify(obj1)}
    and obj2 ${JSON.stringify(obj2)} have the same properties? - ${expected}`, () => {
      assert.equal(task.isSameProperties(obj1, obj2), expected);
    });
  }

  objects.forEach(values => {
    makeTest(values[0], values[1], values[2]);
  });
});
