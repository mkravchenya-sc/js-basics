'use strict';

const task = require('../tasks/task2.08');
const { assert } = require('chai');

describe('task2.08', () => {
  const obj1 = {
    name: 'name1',
    age: 5,
    hobbies: ['skiing', 'reading'],
    isHuman: true,
    pets: null,
    say() {
      console.log('Hi');
    }
  };

  const obj2 = {
    name: 'name1'
  };

  const obj2Swap = {
    name1: 'name'
  };

  const expectedValues = new Map();
  expectedValues.set(obj1, null);
  expectedValues.set(obj2, obj2Swap);

  const makeTest = (obj, expected) => {
    it('Castling of the properties and values of objects must be performed', () => {
      assert.deepEqual(task.castlingIfPossible(obj), expected);
    });
  }

  for (const [obj, expected] of expectedValues.entries()) {
    makeTest(obj, expected);
  }
});
