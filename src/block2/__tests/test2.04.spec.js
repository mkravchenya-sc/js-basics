'use strict';

const task = require('../tasks/task2.04');
const { assert } = require('chai');

describe('task2.04', () => {
  const expectedValues = new Map();
  expectedValues.set([new Date(2018, 7, 27), new Date(2018, 10, 2)], 9);
  expectedValues.set([new Date(2018, 7, 27), new Date(2018, 9, 11)], 6);
  expectedValues.set([new Date(2018, 7, 27), new Date(2018, 8, 24)], 4);

  const makeTest = (birthday, relativeDate, expected) => {
    it(`For ${birthday} of ${relativeDate}, ${expected} weeks have passed`, () => {
      assert.equal(task.ageInWeeks(birthday, relativeDate), expected);
    });
  }

  for (const [dates, expected] of expectedValues.entries()) {
    makeTest(dates[0], dates[1], expected);
  }
});
