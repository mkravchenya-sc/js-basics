'use strict';

const task = require('../tasks/task2.15');
const { assert } = require('chai');

describe('task2.15', () => {
  const persons = [
    {
      name: 'name3', age: 5, isStudent: true
    },
    {
      name: 'name5', age: undefined, isStudent: undefined
    },
    {
      name: undefined, age: 2, isStudent: true
    },
    {
      name: 'name6', isStudent: true
    },
    {
      name: 'name1', age: undefined, isStudent: false
    },
    {
      name: 'name2', age: 3, isStudent: false
    }
  ];

  const obj1 = {
    persons: persons,
    sorterFieldName: 'name',
    sorterFieldDirection: 'ASC'
  };
  const expected1 = [
    {
      name: 'name1', age: undefined, isStudent: false
    },
    {
      name: 'name2', age: 3, isStudent: false
    },
    {
      name: 'name3', age: 5, isStudent: true
    },
    {
      name: 'name5', age: undefined, isStudent: undefined
    },
    {
      name: 'name6', isStudent: true
    },
    {
      name: undefined, age: 2, isStudent: true
    }
  ];

  const obj2 = {
    persons: persons,
    sorterFieldName: 'name',
    sorterFieldDirection: 'DESC'
  };
  const expected2 = [
    {
      name: 'name6', isStudent: true
    },
    {
      name: 'name5', age: undefined, isStudent: undefined
    },
    {
      name: 'name3', age: 5, isStudent: true
    },
    {
      name: 'name2', age: 3, isStudent: false
    },
    {
      name: 'name1', age: undefined, isStudent: false
    },
    {
      name: undefined, age: 2, isStudent: true
    }
  ];

  const obj3 = {
    persons: persons,
    sorterFieldName: 'age',
    sorterFieldDirection: 'ASC'
  };

  // const obj4 = {persons, 'age', 'DESC'};
  // const obj5 = {persons, 'isStudent', 'ASC'};
  // const obj6 = {persons, 'isStudent', 'DESC'};

  const expectedValues = new Map();
  expectedValues.set(obj1, expected1);
  expectedValues.set(obj2, expected2);

  const makeTest = (obj, expected) => {
    it('The sorted array must be as expected', () => {
      assert.deepEqual(task.sortPersons(obj), expected);
    });
  }

  for (const [obj, expected] of expectedValues.entries()) {
    makeTest(obj, expected);
  }

  it('The sorted array should not be as source array', () => {
    assert.notDeepEqual(task.sortPersons(obj3), obj3.persons);
  });
});
