'use strict';

const task = require('../tasks/task2.11');
const { assert } = require('chai');

describe('task2.11', () => {
  const arr = [{ id: 'first', name: 'name' }, { id: 'second' }];

  it(`After changing the properties of objects in the array
    should not change the properties in the massive-like object`, () => {
    const arrayLikeObject = task.toArrayLikeObjectStatic(arr);
    delete arr[0]['name'];
    arr[0]['id'] = 'third';
    arr[1]['name'] = 'name';
    arr.forEach((obj, i) => {
      assert.notDeepEqual(arrayLikeObject[i], obj);
    });
  });
});
