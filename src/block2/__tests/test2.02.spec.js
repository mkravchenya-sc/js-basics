'use strict';

const task = require('../tasks/task2.02');
const { assert } = require('chai');

describe('task2.02', () => {
  const expectedValues = new Map();
  expectedValues.set('String', 'gnirtS');
  expectedValues.set('second STRING', 'GNIRTS dnoces');
  expectedValues.set('Hello world!', '!dlrow olleH');

  const makeTest = (string, expected) => {
    it(`Reverse line for line "${string}" is ${expected}`, () => {
      assert.strictEqual(task.reverse(string), expected);
    });
  }

  for (const [string, expected] of expectedValues.entries()) {
    makeTest(string, expected);
  }
});
