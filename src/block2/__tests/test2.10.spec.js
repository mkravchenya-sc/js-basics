'use strict';

const task = require('../tasks/task2.10');
const { assert } = require('chai');

describe('task2.10', () => {
  const arr = [{ id: 'first', name: 'name' }, { id: 'second' }];

  it(`After changing the properties of objects in the array,
    the properties in the array-like-object should change`, () => {
    const arrayLikeObject = task.toArrayLikeObjectDynamic(arr);
    delete arr[0]['name'];
    arr[0]['id'] = 'third';
    arr[1]['name'] = 'name';
    arr.forEach((obj, i) => {
      assert.deepEqual(arrayLikeObject[i], obj);
    });
  });
});
