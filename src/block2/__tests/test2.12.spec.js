'use strict';

const task = require('../tasks/task2.12');
const { assert } = require('chai');

describe('task2.12', () => {
  it('Expected undefined', () => {
    assert.isUndefined(task.classicAverage(1, 2, true, 4, 5));
  });

  it('Expected undefined', () => {
    assert.isUndefined(task.classicAverage(1, 'dfg', 2, true, 3, '1', 4));
  });

  it('The average value should be as expected', () => {
    assert.strictEqual(task.classicAverage(1, 2, 3, 4, 5), 3);
  });

  it('Expected undefined', () => {
    assert.isUndefined(task.classicAverage(1, 2, 3, 4, ''));
  });
});
