'use strict';

const task = require('../tasks/task2.01');
const { assert } = require('chai');

describe('task2.01', () => {
  const expectedValues = new Map();
  expectedValues.set(-560.52136, 6);
  expectedValues.set(325, 5);
  expectedValues.set(851.13, 3);
  expectedValues.set(-0.21, 1);
  expectedValues.set(24, 4);

  describe('Testing method with using toString', () => {
    const makeTest = (number, expected) => {
      it(`The last digit of the number ${number} is ${expected}`, () => {
        assert.equal(task.lastDigitToString(number), expected);
      });
    }

    for (const [num, expected] of expectedValues.entries()) {
      makeTest(num, expected);
    }
  });

  describe('Testing method without using toString', () => {
    const makeTest = (number, expected) => {
      it(`The last digit of the number ${number} is ${expected}`, () => {
        assert.equal(task.lastDigit(number), expected);
      });
    }

    for (const [num, expected] of expectedValues.entries()) {
      makeTest(num, expected);
    }
  });
});
