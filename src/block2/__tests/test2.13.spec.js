'use strict';

const task = require('../tasks/task2.13');
const { assert } = require('chai');

describe('task2.13', () => {
  it('Expected undefined', () => {
    assert.isUndefined(task.arrowAverage(1, 2, true, 4, 5));
  });

  it('Expected undefined', () => {
    assert.isUndefined(task.arrowAverage(1, 'dfg', 2, true, 3, '1', 4));
  });

  it('The average value should be as expected', () => {
    assert.strictEqual(task.arrowAverage(1, 2, 3, 4, 5), 3);
  });

  it('Expected undefined', () => {
    assert.isUndefined(task.arrowAverage(1, 2, 3, 4, ''));
  });
});
