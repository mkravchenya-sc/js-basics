'use strict';

const task = require('../tasks/task2.09');
const { assert } = require('chai');

describe('task2.09', () => {
  const obj1 = {
    name: 'name1',
    age: 5,
    hobbies: ['skiing', 'reading'],
    isHuman: true,
    pets: null,
    hello() {
      console.log('Hi');
    },
    goodbye() {
      console.log('goodbye');
    }
  };

  const obj2 = {
    say() {
      console.log('say');
    }
  };

  const expectedValues = new Map();
  expectedValues.set(obj1, ['hello', 'goodbye']);
  expectedValues.set(obj2, ['say']);

  const makeTest = (obj, expected) => {
    it('The objects method names must match the expected values', () => {
      assert.deepEqual(task.getMethodsNames(obj), expected);
    });
  }

  for (const [obj, expected] of expectedValues.entries()) {
    makeTest(obj, expected);
  }
});
